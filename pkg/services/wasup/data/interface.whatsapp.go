package data

type WhatsappPayload struct {
	PhoneNumber string `json:"phone_number"`
	Message     string `json:"message"`
}

type WhatsappData struct {
}

type Whatsapp interface {
	SendMessage(phoneNumber string, message string)
}
