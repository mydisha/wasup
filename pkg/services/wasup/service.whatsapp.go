package wasup

import (
	"fmt"
	wa "github.com/Rhymen/go-whatsapp"
	"github.com/mdp/qrterminal"
	"gitlab.com/mydisha/wasup/pkg/services/wasup/data"
	"os"
	"time"
)

var service *WasupService
var wac *wa.Conn
var qrChan = make(chan string)

type WasupService struct {
	data data.WhatsappData
}

func New() *WasupService {
	if service == nil {
		service = new(WasupService)
	}
	return service
}

func init() {
	wac, _ = wa.NewConn(60 * time.Second)

	// Show QrCode on Goroutine
	go GenerateQr()

	wac.Login(qrChan)
}

func (s WasupService) GetConnection() *wa.Conn {
	return wac
}

func GenerateQr() {
	s := fmt.Sprintf("%v\n", <-qrChan)

	qrterminal.Generate(s, qrterminal.L, os.Stdout)
}
