package handler

import (
	"encoding/json"
	"fmt"
	wa "github.com/Rhymen/go-whatsapp"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/mydisha/wasup/pkg/services/wasup"
	"gitlab.com/mydisha/wasup/pkg/services/wasup/data"
	"io/ioutil"
	"net/http"
)

func WhatsappSendHandler(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	body, err := ioutil.ReadAll(request.Body)

	if err != nil {
		http.Error(writer, err.Error(), 500)
		return
	}

	var payload data.WhatsappPayload

	err = json.Unmarshal(body, &payload)

	service := wasup.New()
	text := wa.TextMessage{
		Info: wa.MessageInfo{
			RemoteJid: payload.PhoneNumber + "@s.whatsapp.net",
		},
		Text: payload.Message,
	}

	fmt.Println(service.GetConnection())
	service.GetConnection().Send(text)

	writer.Header().Set("Content-Type", "application/json")
	writer.Write(body)
}
