# wasup

Use Whatsapp to send notification

## Setup
First install required depedency using *dep*
```
dep ensure
```

## Integrate to Whatsapp
Build source code to binary, and run.
In your terminal, Wasup will show generated
QrCode and you need to scan the qrcode using Whatsapp Web
feature on your Whatsapp Application (Android or IOs)
in 1 minute. 