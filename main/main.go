package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/urfave/negroni"
	"gitlab.com/mydisha/wasup/pkg/handler"
	"net/http"
)

func main() {
	router := httprouter.New()
	SetRoute(router)

	n := negroni.Classic() // Includes some default middlewares
	n.UseHandler(router)

	fmt.Println("Starting Wasup...")
	fmt.Println("Scan this QrCode using your Whatsapp.")
	http.ListenAndServe(":8081", n)
}

func SetRoute(router *httprouter.Router) {
	router.POST("/v1/wasup/send", handler.WhatsappSendHandler)
}
